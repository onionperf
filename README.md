# OnionPerf

  * [Overview](#overview)
    + [What does OnionPerf do?](#what-does-onionperf-do-)
    + [What does OnionPerf *not* do?](#what-does-onionperf--not--do-)
  * [Installation](#installation)
    + [Tor](#tor)
    + [TGen](#tgen)
    + [OnionPerf](#onionperf-1)
  * [Documentation](#documentation)
  * [Contributing](#contributing)

## Overview

### What does OnionPerf do?

OnionPerf measures performance of bulk file downloads over Tor. Together with its predecessor, Torperf, OnionPerf has been used to measure long-term performance trends in the Tor network since 2009. It is also being used to perform short-term performance experiments to compare different Tor configurations or implementations.

OnionPerf uses multiple processes and threads to download random data through Tor while tracking the performance of those downloads. The data is served and fetched on localhost using two TGen (traffic generator) processes, and is transferred through Tor using Tor client processes and an ephemeral Tor onion service. Tor control information and TGen performance statistics are logged to disk and analyzed once per day to produce a JSON analysis file that can later be used to visualize changes in Tor client performance over time.

### What does OnionPerf *not* do?

OnionPerf does not attempt to simulate complex traffic patterns like a web-browsing user or a voice-chatting user. It measures a very specific user model: a bulk 5 MiB file download over Tor.

OnionPerf does not interfere with how Tor selects paths and builds circuits, other than setting configuration values as specified by the user. As a result it cannot be used to measure specific relays nor to scan the entire Tor network.

## Installation

OnionPerf has several dependencies in order to perform measurements or analyze and visualize measurement results. These dependencies include Tor, TGen (traffic generator), and a few Python packages.

Development dependencies are tracked in the `requirements.txt` file, while "production" dependencies are tracked in the setup.py file.

The following description was written with a Debian system in mind but should be transferable to other Linux distributions and possibly even other operating systems.

### Tor

OnionPerf relies on the `tor` binary to start a Tor process on the client side to make client requests and another Tor process on the server side to host onion services.

The easiest way to satisfy this dependency is to install the `tor` package, which puts the `tor` binary into the `PATH` where OnionPerf will find it. Optionally, systemd can be instructed to make sure that `tor` is never started as a service:

```shell
sudo apt install tor
sudo systemctl stop tor.service
sudo systemctl mask tor.service
```

Alternatively, Tor can be built from source:

```shell
sudo apt install automake build-essential libevent-dev libssl-dev zlib1g-dev
cd ~/
git clone https://gitlab.torproject.org/tpo/core/tor.git
cd tor/
./autogen.sh
./configure --disable-asciidoc
make
```

In this case the resulting `tor` binary can be found in `~/tor/src/app/tor` and needs to be passed to OnionPerf's `--tor` parameter when doing measurements.

### TGen

OnionPerf uses TGen to generate traffic on client and server side for its measurements. Installing dependencies, cloning TGen to a subdirectory in the user's home directory, and building TGen is done as follows:

```shell
sudo apt install cmake libglib2.0-dev libigraph-dev
cd ~/
git clone https://github.com/shadow/tgen.git
cd tgen/
mkdir build
cd build/
cmake ..
make
```

The TGen binary will be contained in `~/tgen/build/src/tgen`, which is also the path that needs to be passed to OnionPerf's `--tgen` parameter when doing measurements.

### OnionPerf

OnionPerf is written in Python 3. The following instructions assume that a Python virtual environment is being used, even though installation is also possible without that.

The virtual environment is created, activated, and tested using:

```shell
sudo apt install python3-venv
cd ~/
python3 -m venv venv
source venv/bin/activate
which python3
```

The last command should output something like `~/venv/bin/python3` as the path to the `python3` binary used in the virtual environment.

The next step is to clone the OnionPerf repository and install its requirements:

```shell
git clone https://gitlab.torproject.org/tpo/network-health/metrics/onionperf.git
pip3 install --no-cache -r onionperf/requirements.txt
```

The final step is to install OnionPerf and print out the usage information to see if the installation was successful:

```shell
cd onionperf/
pip install -i .
cd ~/
onionperf --help
```

The virtual environment is deactivated with the following command:

```shell
deactivate
```

However, in order to perform measurements or analyses, the virtual environment needs to be activated first. This will ensure all the paths are found.

If needed, unit tests are run with the following command:

```shell
cd ~/onionperf/
python3 -m nose --with-coverage --cover-package=onionperf
```

## Documentation

Docs are available on:

[HTML](https://tpo.pages.torproject.net/network-health/metrics/onionperf/)

[Source](https://gitlab.torproject.org/tpo/network-health/metrics/onionperf/-/tree/master/docs)


## Contributing

The OnionPerf code is developed at https://gitlab.torproject.org/tpo/network-health/metrics/onionperf.

Contributions to OnionPerf are welcome and encouraged!
